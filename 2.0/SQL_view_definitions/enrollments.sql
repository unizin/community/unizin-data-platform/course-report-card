CREATE SCHEMA IF NOT EXISTS f_report_card;

DROP MATERIALIZED VIEW IF EXISTS f_report_card.enrollments;

CREATE MATERIALIZED VIEW f_report_card.enrollments
TABLESPACE pg_default
AS SELECT DISTINCT co.title AS course_title,
    co.subject AS course_subject,
    co.number AS course_number,
    co.subject || co.number AS course_code,
    act.name AS term_name,
    act.term_type,
    act.term_year,
    act.term_begin_date,
    p.person_id,
        CASE
            WHEN p.sex = 'Female'::text THEN true
            ELSE false
        END AS is_female,
    p.is_first_generation_hed_student AS is_first_time_hed,
        CASE
            WHEN p.citizenship_one <> 'US'::text AND p.citizenship_one IS NOT NULL THEN true
            ELSE false
        END AS is_international,
        CASE
            WHEN p.is_white = false AND p.is_asian = false AND p.is_no_race_indicated = false AND (p.is_american_indian_alaska_native = true OR p.is_black_african_american = true OR p.is_hawaiian_pacific_islander = true OR p.is_hispanic_latino = true OR p.is_other_american = true) THEN true
            ELSE false
        END AS is_urm
   FROM entity.course_section_enrollment cse
     JOIN entity.person p ON cse.person_id = p.person_id
     LEFT JOIN entity.course_section cs ON cs.course_section_id = cse.course_section_id
     LEFT JOIN entity.course_offering co ON co.course_offering_id = cs.course_offering_id
     LEFT JOIN entity.academic_session acs ON acs.academic_session_id = co.academic_session_id
     LEFT JOIN entity.academic_term act ON act.academic_term_id = acs.academic_term_id
  WHERE cse.role = 'Student'::text AND cse.role_status = 'Enrolled'::text
WITH DATA;