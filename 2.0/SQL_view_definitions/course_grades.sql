CREATE SCHEMA IF NOT EXISTS f_report_card;

DROP MATERIALIZED VIEW IF EXISTS f_report_card.course_grades;

CREATE MATERIALIZED VIEW f_report_card.course_grades
TABLESPACE pg_default
AS SELECT act.name AS term_name,
    act.term_type,
    act.term_begin_date,
    co.subject AS course_subject,
    co.number AS course_number,
    co.subject || co.number AS course_code,
    cg.grade_inputted_by_instructor,
    cg.grade_on_official_transcript,
    cg.grade_points_per_credit,
    cg.grade_points,
    cg.person_id
   FROM entity.course_grade cg
     LEFT JOIN entity.course_section cs ON cs.course_section_id = cg.course_section_id
     JOIN entity.course_section_enrollment cse ON cse.person_id = cg.person_id AND cse.course_section_id = cs.course_section_id
     LEFT JOIN entity.course_offering co ON co.course_offering_id = cs.course_offering_id
     LEFT JOIN entity.academic_session acs ON acs.academic_session_id = co.academic_session_id
     LEFT JOIN entity.academic_term act ON act.academic_term_id = acs.academic_term_id
WITH DATA;